# nextgrowth-assesment

Video Link : (https://www.youtube.com/watch?v=wpl7CYI1m8c)
## Section-A

Making a simple website with JS

Created a simple website using react.

Main repository :  (https://gitlab.com/sagar200/section-a)

Live Website : (https://cloud-storage-01.netlify.app/)

Form Data populated here : (https://forms.maakeetoo.com/formsdata/119)


## Section-B

### Task-1

Folder structer like a tree node

Main repository : (https://gitlab.com/sagar200/section-b-task1/-/tree/master)

Live Website : (https://tree-folder-structure1.netlify.app/)
### Task-2

Lazy Loading to avoid pagination.

Used React hooks to create a lazy loading effect.

Main repository :  (https://gitlab.com/sagar200/section-b-task2)

Live Website :  (https://lazy-loading-effect.netlify.app/)

API used  : (https://api.unsplash.com/photos)



## Section-C

Suppose you are building a financial planning tool - which requires us to fetch bank statements from the user. Now, we would like to encrypt the data - so that nobody - not even the developer can view it. What would you do to solve this problem?

ans: mainly there are 2 two types of encryption techniques. 
    
    
    
     1. one way encryption
     2. two way encryption



one way encryption method is non reversible method so the best solution for the above problem is using one way encryption because in that once the data or the value is hashed it cannot se retrieve back.


## Bonus

Is there any way that you can automate the deployment?

ans: I think using jenkins(CI/CD) tools we can automate the deployment.




